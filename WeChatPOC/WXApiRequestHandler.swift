//
//  WXApiRequestHandler.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

import Foundation
class WXApiRequestHandler {
   
   /// Method sendLinkURL to share links
   ///
   /// - Parameters:
   ///   - urlString: URL of the webpage. It can not be left blank and the size can not exceed 10K.
   ///   - tagName: mediaTagName; contents can not exceed 64 bytes
   ///   - title: title. Its contents can not exceed 512 bytes
   ///   - description: description. Its contents can not exceed 1k
   ///   - thumbImage: image thumb. Its contents can not exceed 32K
   ///   - scene: Target scene, you can send to chat, contact or moments. Contact if default choice.
   /// - Returns: Yes (success) or No (failure)
   class func sendLinkURL(_ urlString: String, tagName: String, title: String, description: String, thumbImage: UIImage, scene: WXScene) -> Bool {
      let ext = WXWebpageObject()
      ext.webpageUrl = urlString
      let message = WXMediaMessage.messageFactory(title: title, description: description, mediaObject: ext, thumbImage: thumbImage, mediaTagName: tagName)
      let req = SendMessageToWXReq.sendMessageFactory(message: message, bText: false, scene: scene)
      return WXApi.send(req)
   }
}
