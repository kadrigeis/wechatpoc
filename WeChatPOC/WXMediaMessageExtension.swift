//
//  WXMediaMessageExtension.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

extension WXMediaMessage {
   class func messageFactory(title: String, description: String, mediaObject: Any, thumbImage: UIImage, mediaTagName: String)->WXMediaMessage {
      let message = WXMediaMessage()
      message.title = title
      message.description = description
      message.mediaObject = mediaObject
      message.mediaTagName = mediaTagName
      message.setThumbImage(thumbImage)
      return message
   }
}
