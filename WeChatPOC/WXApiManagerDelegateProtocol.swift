//
//  WXApiManagerDelegateProtocol.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

@objc protocol WXApiManagerDelegateProtocol : class {
   @objc optional func managerDidRecvMessageResponse(response : SendMessageToWXResp)
   @objc optional func managerDidRecvAuthResponse(response : SendAuthResp)
   @objc optional func managerDidRecvAddCardResponse(response : AddCardToWXCardPackageResp)
}
