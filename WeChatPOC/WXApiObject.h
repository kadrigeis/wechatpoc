//
//  MMApiObject.h
//  Api对象，包含所有接口和对象数据定义
//
//  Created by Wechat on 12-2-28.
//  Copyright (c) 2012年 Tencent. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @brief 错误码
 *
 */
enum  WXErrCode {
    WXSuccess           = 0,    /**< Successfull    */
    WXErrCodeCommon     = -1,   /**< Login Error Type    */
    WXErrCodeUserCancel = -2,   /**< The user clicks the Cancel and return    */
    WXErrCodeSentFail   = -3,   /**< Send Failed    */
    WXErrCodeAuthDeny   = -4,   /**< Authorization failed    */
    WXErrCodeUnsupport  = -5,   /**< Error not supported    */
};



/*! @brief 请求发送场景
 *
 */
enum WXScene {
    WXSceneSession  = 0,        /**< Interface de chat    */
    WXSceneTimeline = 1,        /**< Moments       */
    WXSceneFavorite = 2,        /**< Favoris       */
};



enum WXAPISupport {
    WXAPISupportSession = 0,
};



/*! @brief Profile type
 *
 */
enum WXBizProfileType{
    WXBizProfileType_Normal = 0,    //**< general public  */
    WXBizProfileType_Device = 1,    //**< The public of hardware  */
};



/*! @brief Web Page Type
 *
 */
enum WXMPWebviewType {
    WXMPWebviewType_Ad = 0,        /**< Advertising Web Page **/
};


/*! @brief File Type
 *
 */
typedef NS_ENUM(UInt64, enAppSupportContentFlag)
{
    MMAPP_SUPPORT_NOCONTENT = 0x0,
    MMAPP_SUPPORT_TEXT      = 0x1,
    MMAPP_SUPPORT_PICTURE   = 0x2,
    MMAPP_SUPPORT_LOCATION  = 0x4,
    MMAPP_SUPPORT_VIDEO     = 0x8,
    MMAPP_SUPPORT_AUDIO     = 0x10,
    MMAPP_SUPPORT_WEBPAGE   = 0x20,
    
    // Suport File Type
    MMAPP_SUPPORT_DOC  = 0x40,               // doc
    MMAPP_SUPPORT_DOCX = 0x80,               // docx
    MMAPP_SUPPORT_PPT  = 0x100,              // ppt
    MMAPP_SUPPORT_PPTX = 0x200,              // pptx
    MMAPP_SUPPORT_XLS  = 0x400,              // xls
    MMAPP_SUPPORT_XLSX = 0x800,              // xlsx
    MMAPP_SUPPORT_PDF  = 0x1000,             // pdf
};

#pragma mark - BaseReq
/*! @brief Basic class of all request classes of WeChat SDK
 *
 */
@interface BaseReq : NSObject

/** Request type */
@property (nonatomic, assign) int type;
/**
 * The unique identifier, comprised of a user’s WeChat ID and App ID, must be filled by developers while sending a request.
 * The purpose is to check whether the user has logged in with another account.
 */
@property (nonatomic, retain) NSString* openID;

@end



#pragma mark - BaseResp
/*! @brief Basic class of all response classes of WeChat SDK
 *
 */
@interface BaseResp : NSObject
/** Error code */
@property (nonatomic, assign) int errCode;
/** Error notification string */
@property (nonatomic, retain) NSString *errStr;
/** Response type */
@property (nonatomic, assign) int type;

@end



#pragma mark - WXMediaMessage
@class WXMediaMessage;

/*! @brief 第三方向微信终端发起支付的消息结构体
 *
 *  第三方向微信终端发起支付的消息结构体，微信终端处理后会向第三方返回处理结果
 * @see PayResp
 */
@interface PayReq : BaseReq

/** 商家向财付通申请的商家id */
@property (nonatomic, retain) NSString *partnerId;
/** 预支付订单 */
@property (nonatomic, retain) NSString *prepayId;
/** 随机串，防重发 */
@property (nonatomic, retain) NSString *nonceStr;
/** 时间戳，防重发 */
@property (nonatomic, assign) UInt32 timeStamp;
/** 商家根据财付通文档填写的数据和签名 */
@property (nonatomic, retain) NSString *package;
/** 商家根据微信开放平台文档对数据做的签名 */
@property (nonatomic, retain) NSString *sign;

@end



#pragma mark - PayResp
/*! @brief 微信终端返回给第三方的关于支付结果的结构体
 *
 *  微信终端返回给第三方的关于支付结果的结构体
 */
@interface PayResp : BaseResp

/** 财付通返回给商家的信息 */
@property (nonatomic, retain) NSString *returnKey;

@end



/*! @brief 第三方向微信终端发起拆企业红包的消息结构体
 *
 *  第三方向微信终端发起拆企业红包的消息结构体，微信终端处理后会向第三方返回处理结果
 * @see HBReq
 */
@interface HBReq : BaseReq

/** 随机串，防重发 */
@property (nonatomic, retain) NSString *nonceStr;
/** 时间戳，防重发 */
@property (nonatomic, assign) UInt32 timeStamp;
/** 商家根据微信企业红包开发文档填写的数据和签名 */
@property (nonatomic, retain) NSString *package;
/** 商家根据微信企业红包开发文档对数据做的签名 */
@property (nonatomic, retain) NSString *sign;

@end



#pragma mark - HBResp
/*! @brief 微信终端返回给第三方的关于拆企业红包结果的结构体
 *
 *  微信终端返回给第三方的关于拆企业红包结果的结构体
 */
@interface HBResp : BaseResp

@end




#pragma mark - SendAuthReq
/*! @brief 第三方程序向微信终端请求认证的消息结构
 *
 * 第三方程序要向微信申请认证，并请求某些权限，需要调用WXApi的sendReq成员函数，
 * 向微信终端发送一个SendAuthReq消息结构。微信终端处理完后会向第三方程序发送一个处理结果。
 * @see SendAuthResp
 */
@interface SendAuthReq : BaseReq
/** 第三方程序要向微信申请认证，并请求某些权限，需要调用WXApi的sendReq成员函数，向微信终端发送一个SendAuthReq消息结构。微信终端处理完后会向第三方程序发送一个处理结果。
 * @see SendAuthResp
 * @note scope字符串长度不能超过1K
 */
@property (nonatomic, retain) NSString* scope;
/** 第三方程序本身用来标识其请求的唯一性，最后跳转回第三方程序时，由微信终端回传。
 * @note state字符串长度不能超过1K
 */
@property (nonatomic, retain) NSString* state;
@end



#pragma mark - SendAuthResp
/*! @brief 微信处理完第三方程序的认证和权限申请后向第三方程序回送的处理结果。
 *
 * 第三方程序要向微信申请认证，并请求某些权限，需要调用WXApi的sendReq成员函数，向微信终端发送一个SendAuthReq消息结构。
 * 微信终端处理完后会向第三方程序发送一个SendAuthResp。
 * @see onResp
 */
@interface SendAuthResp : BaseResp
@property (nonatomic, retain) NSString* code;
/** 第三方程序发送时用来标识其请求的唯一性的标志，由第三方程序调用sendReq时传入，由微信终端回传
 * @note state字符串长度不能超过1K
 */
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* lang;
@property (nonatomic, retain) NSString* country;
@end



#pragma mark - SendMessageToWXReq
/*! @brief Message structure that the third-party application uses to send message to WeChat
 *
 * The third-party app uses SendMessageToWXReq to send messages to WeChat.
 * The message type can be text (member: text) and multi-media (member: message).
 * WeChat will then return the result after processing.
 * @see SendMessageToWXResp
 */
@interface SendMessageToWXReq : BaseReq
/** Text contents in the message sent
 * @note The size of texts should be within 0-10k.
 */
@property (nonatomic, retain) NSString* text;
/** Multi-media contents in the message sent
 * @see WXMediaMessage
 */
@property (nonatomic, retain) WXMediaMessage* message;
/** The message type can be Text or Multi-media but not both. */
@property (nonatomic, assign) BOOL bText;
/**  Target scene, you can send to contact or moments. Contact if default choice.
 * @see WXScene
 */
@property (nonatomic, assign) int scene;

@end



#pragma mark - SendMessageToWXResp
/*! @brief Result of SendMessageToWXReq that WeChat returns to the third-party app
 *
 * Wechat uses SendMessageToWXResp to return results of SendMessageToWXReq from the third-party app.
 */
@interface SendMessageToWXResp : BaseResp
@property(nonatomic, retain) NSString* lang;
@property(nonatomic, retain) NSString* country;
@end



#pragma mark - GetMessageFromWXReq
/*! @brief Message structure that the third-party application uses to request authorization from WeChat
 *
 * The third-party app requests for verification and authorization by calling sendReq member function of WXApi.
 * WeChat will return a result after processing.
 
 */
@interface GetMessageFromWXReq : BaseReq
@property (nonatomic, retain) NSString* lang;
@property (nonatomic, retain) NSString* country;
@end



#pragma mark - GetMessageFromWXResp
/*! @brief Message structure that the third-party application uses to response requests from WeChat
 *
 * WeChat sends a request to a third-party application;
 * and the third-party application calls sendResp to return the result in a GetMessageFromWXResp message.
 */
@interface GetMessageFromWXResp : BaseResp
/** Text contents provided to WeChat
 @note The size of texts should be within 0-10k.
 */
@property (nonatomic, retain) NSString* text;
/** Multi-media contents provided to WeChat
 * @see WXMediaMessage
 */
@property (nonatomic, retain) WXMediaMessage* message;
/** Types of message that providing contents to WeChat. It could be text or multi-media but not both. */
@property (nonatomic, assign) BOOL bText;
@end



#pragma mark - ShowMessageFromWXReq
/*! @brief WeChat asks the third-party to show contents
 *
 * WeChat sends an ShowMessageFromWXReq message to ask the third-party app to show certain contents.
 * And the third-party app calls sendResp to send an ShowMessageFromWXResp message to WeChat after processing.
 */
@interface ShowMessageFromWXReq : BaseReq
/** WeChat asks the third-party to show contents
 * @see WXMediaMessage
 */
@property (nonatomic, retain) WXMediaMessage* message;
@property (nonatomic, retain) NSString* lang;
@property (nonatomic, retain) NSString* country;
@end



#pragma mark - ShowMessageFromWXResp
/*! @brief WeChat sends an ShowMessageFromWXReq message to ask the third-party app to
 * show certain contents.
 * And the third-party app calls sendResp to send an ShowMessageFromWXResp message to
 * WeChat after processing.
 */
@interface ShowMessageFromWXResp : BaseResp
@end



#pragma mark - LaunchFromWXReq
/*! @brief The message structure carried by WeChat client when a third-party application is opened
 *
 *  This is the WeChat’s data structure sent to a third party. No return value expected from the third party.
 */
@interface LaunchFromWXReq : BaseReq
@property (nonatomic, retain) WXMediaMessage* message;
@property (nonatomic, retain) NSString* lang;
@property (nonatomic, retain) NSString* country;
@end

#pragma mark - OpenTempSessionReq
/* ! @brief 第三方通知微信，打开临时会话
 *
 * 第三方通知微信，打开临时会话
 */
@interface OpenTempSessionReq : BaseReq
/** 需要打开的用户名
 * @attention 长度不能超过512字节
 */
@property (nonatomic, retain) NSString* username;
/** 开发者自定义参数，拉起临时会话后会发给开发者后台，可以用于识别场景
 * @attention 长度不能超过32位
 */
@property (nonatomic, retain) NSString*  sessionFrom;
@end

#pragma mark - OpenWebviewReq
/* ! @brief 第三方通知微信启动内部浏览器，打开指定网页
 *
 *  第三方通知微信启动内部浏览器，打开指定Url对应的网页
 */
@interface OpenWebviewReq : BaseReq
/** 需要打开的网页对应的Url
 * @attention 长度不能超过1024
 */
@property(nonatomic,retain)NSString* url;

@end

#pragma mark - OpenWebviewResp
/*! @brief 微信终端向第三方程序返回的OpenWebviewReq处理结果
 *
 * 第三方程序向微信终端发送OpenWebviewReq后，微信发送回来的处理结果，该结果用OpenWebviewResp表示
 */
@interface OpenWebviewResp : BaseResp

@end


#pragma mark - OpenTempSessionResp
/*! @brief 微信终端向第三方程序返回的OpenTempSessionReq处理结果。
 *
 * 第三方程序向微信终端发送OpenTempSessionReq后，微信发送回来的处理结果，该结果用OpenTempSessionResp表示。
 */
@interface OpenTempSessionResp : BaseResp

@end

#pragma mark - OpenRankListReq
/* ! @brief 第三方通知微信，打开硬件排行榜
 *
 * 第三方通知微信，打开硬件排行榜
 */
@interface OpenRankListReq : BaseReq

@end

#pragma mark - OpenRanklistResp
/*! @brief 微信终端向第三方程序返回的OpenRankListReq处理结果。
 *
 * 第三方程序向微信终端发送OpenRankListReq后，微信发送回来的处理结果，该结果用OpenRankListResp表示。
 */
@interface OpenRankListResp : BaseResp

@end

#pragma mark - JumpToBizProfileReq
/* ! @brief 第三方通知微信，打开指定微信号profile页面
 *
 * 第三方通知微信，打开指定微信号profile页面
 */
@interface JumpToBizProfileReq : BaseReq
/** 跳转到该公众号的profile
 * @attention 长度不能超过512字节
 */
@property (nonatomic, retain) NSString* username;
/** 如果用户加了该公众号为好友，extMsg会上传到服务器
 * @attention 长度不能超过1024字节
 */
@property (nonatomic, retain) NSString* extMsg;
/**
 * 跳转的公众号类型
 * @see WXBizProfileType
 */
@property (nonatomic, assign) int profileType;
@end



#pragma mark - JumpToBizWebviewReq
/* ! @brief 第三方通知微信，打开指定usrname的profile网页版
 *
 */
@interface JumpToBizWebviewReq : BaseReq
/** 跳转的网页类型，目前只支持广告页
 * @see WXMPWebviewType
 */
@property(nonatomic, assign) int webType;
/** 跳转到该公众号的profile网页版
 * @attention 长度不能超过512字节
 */
@property(nonatomic, retain) NSString* tousrname;
/** 如果用户加了该公众号为好友，extMsg会上传到服务器
 * @attention 长度不能超过1024字节
 */
@property(nonatomic, retain) NSString* extMsg;

@end

#pragma mark - WXCardItem

@interface WXCardItem : NSObject
/** 卡id
 * @attention 长度不能超过1024字节
 */
@property (nonatomic,retain) NSString* cardId;
/** ext信息
 * @attention 长度不能超过2024字节
 */
@property (nonatomic,retain) NSString* extMsg;
/**
 * @attention 卡的状态,req不需要填。resp:0为未添加，1为已添加。
 */
@property (nonatomic,assign) UInt32 cardState;
/**
 * @attention req不需要填，chooseCard返回的。
 */
@property (nonatomic,retain) NSString* encryptCode;
/**
 * @attention req不需要填，chooseCard返回的。
 */
@property (nonatomic,retain) NSString* appID;
@end;

#pragma mark - AddCardToWXCardPackageReq
/* ! @brief 请求添加卡券至微信卡包
 *
 */

@interface AddCardToWXCardPackageReq : BaseReq
/** 卡列表
 * @attention 个数不能超过40个 类型WXCardItem
 */
@property (nonatomic,retain) NSArray* cardAry;

@end


#pragma mark - AddCardToWXCardPackageResp
/** ! @brief 微信返回第三方添加卡券结果
 *
 */

@interface AddCardToWXCardPackageResp : BaseResp
/** 卡列表
 * @attention 个数不能超过40个 类型WXCardItem
 */
@property (nonatomic,retain) NSArray* cardAry;
@end

#pragma mark - WXChooseCardReq
/* ! @brief 请求从微信选取卡券
 *
 */

@interface WXChooseCardReq : BaseReq
@property(nonatomic, strong) NSString *appID;
@property(nonatomic, assign) UInt32 shopID;
@property(nonatomic, assign) UInt32 canMultiSelect;
@property(nonatomic, strong) NSString *cardType;
@property(nonatomic, strong) NSString *cardTpID;
@property(nonatomic, strong) NSString *signType;
@property(nonatomic, strong) NSString *cardSign;
@property(nonatomic, assign) UInt32 timeStamp;
@property(nonatomic, strong) NSString *nonceStr;
@end


#pragma mark - WXChooseCardResp
/** ! @brief 微信返回第三方请求选择卡券结果
 *
 */

@interface WXChooseCardResp : BaseResp
@property (nonatomic,retain) NSArray* cardAry;
@end

#pragma mark - WXMediaMessage

/** Multi-media contents in the message sent
 * @see WXMediaMessage
 */
@interface WXMediaMessage : NSObject

+(WXMediaMessage *) message;

/** Title
 * @note contents can not exceed 512 bytes
 */
@property (nonatomic, retain) NSString *title;
/** Description
 * @note contents can not exceed 1k
 */
@property (nonatomic, retain) NSString *description;
/** Data of the thumb
 * @note contents can not exceed 32K
 */
@property (nonatomic, retain) NSData   *thumbData;
/** todo
 * @note contents can not exceed 64 bytes
 */
@property (nonatomic, retain) NSString *mediaTagName;
/**
 *
 */
@property (nonatomic, retain) NSString *messageExt;
@property (nonatomic, retain) NSString *messageAction;
/** Multi-media data object, including WXWebpageObject, WXImageObject, WXMusicObject, etc. */
@property (nonatomic, retain) id        mediaObject;

/*! @brief Method used to set the thumb of image message
 *
 * @param image Thumb
 * @note contents can not exceed 32K
 */
- (void) setThumbImage:(UIImage *)image;

@end



#pragma mark - WXImageObject
/*! @brief Image object included in multi-media messages
 *
 * The image object included in the message transferred between WeChat and the third-party app
 * @note imageData and imageUrl can not be left blank at the same time.
 * @see WXMediaMessage
 */
@interface WXImageObject : NSObject
/*! @brief Return a WXImageObject object
 *
 * @note The WXImageObject object returned is auto-released.
 */
+(WXImageObject *) object;

/** Actual contents of the image
 * @note file size can not exceed 10M.
 */
@property (nonatomic, retain) NSData    *imageData;

@end


#pragma mark - WXMusicObject
/*! @brief Music object included in multi-media messages
 *
 * Music object included in the message transferred between WeChat and the third-party app
 * @note musicUrl and musicLowBandUrl member can not be left blank at the same time.
 * @see WXMediaMessage
 */
@interface WXMusicObject : NSObject
/*! @brief Return a WXMusicObject object
 *
 * @note The WXMusicObject object returned is auto-released.
 */
+(WXMusicObject *) object;

/** URL of music webpage
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *musicUrl;
/** URL of music lowband webpage
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *musicLowBandUrl;
/** URL of music data
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *musicDataUrl;

/** URL of lowband data of the music
 * @note  contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *musicLowBandDataUrl;

@end



#pragma mark - WXVideoObject
/*! @brief Video object included in multi-media messages
 *
 * Video object included in the message transferred between WeChat and the third-party app
 * @note videoUrl and videoLowBandUrl can not be left blank at the same time.
 * @see WXMediaMessage
 */
@interface WXVideoObject : NSObject
/*! @brief Returns a WXVideoObject object
 *
 * @note The WXVideoObject object returned is automatically released.
 */
+(WXVideoObject *) object;

/** URL of video data
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *videoUrl;
/** URL of video lowband data
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *videoLowBandUrl;

@end



#pragma mark - WXWebpageObject
/*! @brief Webpage object included in multi-media messages
 *
 * Webpage object included in the multi-media message transferred between WeChat and the third-party app
 * @see WXMediaMessage
 */
@interface WXWebpageObject : NSObject
/*! @brief Return a WXWebpageObject object
 *
 * @note The WXWebpageObject object returned is auto-released.
 */
+(WXWebpageObject *) object;

/** URL of the webpage
 * @note It can not be left blank and the size can not exceed 10K.
 */
@property (nonatomic, retain) NSString *webpageUrl;

@end



#pragma mark - WXAppExtendObject
/*! @brief App extend object included in multi-media messages
 *
 * The third-party app sends a multi-media message that includes WXAppExtendObject to WeChat.
 * WeChat calls this app to process the multi-media contents.
 * @note url, extInfo and fileData can not be left blank at the same time.
 * @see WXMediaMessage
 */
@interface WXAppExtendObject : NSObject
/*! @brief Return a WXAppExtendObject object
 *
 * @note The WXAppExtendObject object returned is auto-released.
 */
+(WXAppExtendObject *) object;

/** If the third-party app does not exist, WeChat will open the download URL of the app.
 * @note contents can not exceed 10K
 */
@property (nonatomic, retain) NSString *url;
/** Custom data of the third-party app. WeChat will return it to the app for processing.
 * @note contents can not exceed 2K
 */
@property (nonatomic, retain) NSString *extInfo;
/** App file data. When this data is sent to WeChat contacts, the contact needs to click to download.
 * WeChat then returns it to the app for processing.
 * @note file size can not exceed 10M.
 */
@property (nonatomic, retain) NSData   *fileData;

@end



#pragma mark - WXEmoticonObject
/*! @brief Emoticon object included in multi-media messages transferred between WeChat and the third-party app
 *
 * @see WXMediaMessage
 */
@interface WXEmoticonObject : NSObject

/*! @brief Return a WXEmoticonObject object
 *
 * @note The WXEmoticonObject object returned is auto-released.
 */
+(WXEmoticonObject *) object;

/** The content of emoticon
 * @note contents can not exceed 10M.
 */
@property (nonatomic, retain) NSData    *emoticonData;

@end



#pragma mark - WXFileObject
/*! @brief todo
 *
 * @see WXMediaMessage
 */
@interface WXFileObject : NSObject

/*! @brief Returns a WXFileObject object
 *
 * @note The WXFileObject object returned is automatically released.
 */
+(WXFileObject *) object;

/** File Extensions
 * @note Must be less than 64 bytes
 */
@property (nonatomic, retain) NSString  *fileExtension;

/** File content
 * @note Must be less than 64 bytes
 */
@property (nonatomic, retain) NSData    *fileData;

@end


#pragma mark - WXLocationObject
/*! @brief 多媒体消息中包含的地理位置数据对象
 *
 * 微信终端和第三方程序之间传递消息中包含的地理位置数据对象。
 * @see WXMediaMessage
 */
@interface WXLocationObject : NSObject

/*! @brief 返回一个WXLocationObject对象
 *
 * @note 返回的WXLocationObject对象是自动释放的
 */
+(WXLocationObject *) object;

/** 地理位置信息
 * @note 经纬度
 */
@property (nonatomic, assign) double lng; //经度
@property (nonatomic, assign) double lat; //纬度

@end


#pragma mark - WXTextObject
/*! @brief 多媒体消息中包含的文本数据对象
 *
 * 微信终端和第三方程序之间传递消息中包含的文本数据对象。
 * @see WXMediaMessage
 */
@interface WXTextObject : NSObject

/*! @brief 返回一个WXTextObject对象
 *
 * @note 返回的WXTextObject对象是自动释放的
 */
+(WXTextObject *) object;

/** 地理位置信息
 * @note 文本内容
 */
@property (nonatomic, retain) NSString *contentText;

@end

