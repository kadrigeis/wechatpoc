//
//  ViewController.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WXApiManagerDelegateProtocol {
   static let kLinkURL = "http://tech.qq.com/zt2012/tmtdecode/252.htm"
   static let kLinkTagName = "WECHAT_TAG_JUMP_SHOWRANK"
   static let kLinkTitle = "Interview exclusif avec Zhang Bruce Lee"
   static let kLinkDescription = "Description de l'interview"
   
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      WXApiManager.sharedInstance().delegate = self
   }

   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   @IBAction func sendLinkAction(_ sender: UIButton) {
      self.sendLink()
   }
   
   func sendLink() {
      guard let thumbImage = UIImage(named: "res2.png") else {
         return;
      }
      let currentScene = WXSceneSession
      let result = WXApiRequestHandler.sendLinkURL(ViewController.kLinkURL, tagName: ViewController.kLinkTagName, title: ViewController.kLinkTitle, description: ViewController.kLinkDescription, thumbImage: thumbImage, scene: currentScene)
      if result {
         print("Ok")
      } else {
         print("Nothing happened")
      }
   }
   
   func managerDidRecvMessageResponse(response : SendMessageToWXResp) {
      let title = "Résultat de l'envoie d'un message médiatique"
      let msg = "errCode:\(response.errCode)"
      let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }
   
   func managerDidRecvAuthResponse(response : SendAuthResp) {
      let title = "Résultat de l'auth"
      let msg = "code:\(response.errCode), state:\(response.state), errcode:\(response.errCode))"
      let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }

   func managerDidRecvAddCardResponse(response : AddCardToWXCardPackageResp) {
      var cardStr = ""
      for item in response.cardAry {
         if let cardItem = item as? WXCardItem {
         cardStr += "cardid:\(cardItem.cardId) cardext:\(cardItem.extMsg) cardstate:\(cardItem.cardState)\n"
         }
      }
      let title = "add card resp"
      let alert = UIAlertController(title: title, message: cardStr, preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }


}

