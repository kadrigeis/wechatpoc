//
//  WXApiManager.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

class WXApiManager : NSObject, WXApiDelegate {
   static let _sharedInstance = WXApiManager()
   var delegate: WXApiManagerDelegateProtocol?
   
   private override init() {}
   
   class func sharedInstance() -> WXApiManager {
      return WXApiManager._sharedInstance
   }

   func onResp(_ resp: BaseResp!) {
      if resp is SendMessageToWXResp {
         guard let method = delegate?.managerDidRecvMessageResponse else {
            return
         }
         let messageResp = resp as! SendMessageToWXResp
         method(messageResp)
      } else if resp is SendAuthResp {
         guard let method = delegate?.managerDidRecvAuthResponse else {
            return
         }
         let messageResp = resp as! SendAuthResp
         method(messageResp)
      } else if resp is AddCardToWXCardPackageResp {
         guard let method = delegate?.managerDidRecvAddCardResponse else {
            return
         }
         let messageResp = resp as! AddCardToWXCardPackageResp
         method(messageResp)
      }
   }

}
