//
//  SendMessageToWXReqExtension.swift
//  WeChatPOC
//
//  Created by Eric Jadas-Hécart on 05/05/2017.
//  Copyright © 2017 Eric Jadas-Hécart. All rights reserved.
//

extension SendMessageToWXReq {
   /// Factory of SendMessageToWXReq
   ///
   /// - Parameters:
   ///   - message: Multi-media contents in the message sent
   ///   - bText: if true the message type is Text otherwise it is Multi-media
   ///   - scene: Target scene, you can send to chat, contact or moments. Contact if default choice.
   /// - Returns: SendMessageToWXReq instance
   class func sendMessageFactory(message: WXMediaMessage, bText: Bool, scene: WXScene) -> SendMessageToWXReq {
      let req = SendMessageToWXReq()
      req.bText = bText
      req.scene = Int32(scene.rawValue)
      req.message = message
      return req
   }
}
